package com.sharepoint.sharepoint;//package hk.quantr.sharepoint;
//
import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import hk.quantr.peterswing.CommonLib;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

//
///**
// *
// * @author Peter <peter@quantr.hk>
// */
@RestController
public class SpringController {

	final static String domain  = "medicaltti";
	final static String username = "admin@medicaltti.onmicrosoft.com";
	final static String password = "Jarcgroup12345@";


	@RequestMapping("/get")
	@PostMapping
	public void testUpload(@RequestParam String domain, @RequestParam String username, @RequestParam String password) {

			try {
				downloadFile("https://medicaltti.sharepoint.com/Shared%Documents/User-Acceptance-Testing-Test-Case-Ecommerce-IDEXX.xlsx","test");
			}catch (Exception e){
				System.out.println(e);
			}
	}


	@RequestMapping("/getToken")
	@PostMapping
	public String tokens(@RequestParam String domain, @RequestParam String username, @RequestParam String password) {
		if (domain == null || username == null || password == null) {
			return "You need to pass : domain, username and password";
		} else {
			Pair<String, String> token = SPOnline.login(username, password, domain);
			if (token == null) {
				return "Authentication failed " + domain + ", " + username + ", " + password;
			} else {
				return token.getLeft() + "\n" + token.getRight();
			}
		}
	}
	@RequestMapping("/getFiles")
	@PostMapping
	public void getItemsFromDocLib() {
		try {
//			Logger.getLogger(SPOnline.class).info("get all files with associated items from document library");
			//String view = "peter view呀";
			String site = "";
			String docLib = "_layouts/Doc.aspx";

//			List<String> lines = IOUtils.readLines(new FileReader(System.getProperty("user.home") + File.separator + "password.txt"));

			Pair<String, String> token = SPOnline.login(username, password, domain);
			if (token != null) {
				JSONObject json;
				String jsonString;

//				// get folder by specific an uniqueId
				ArrayList<String> guids = new ArrayList();
//				jsonString = SPOnline.get(token, domain, "_api/web/GetFolderByServerRelativeUrl('Shared%20Documents')/Files?$expand=ListItemAllFields&$filter=" + URLEncoder.encode("filterListItemAllFields/GUID ne null", "utf-8"));
//				if (jsonString != null) {
//					System.out.println(CommonLib.prettyFormatJson(jsonString));
//					json = new JSONObject(jsonString);
//					JSONArray arr = json.getJSONObject("d").getJSONArray("results");
//					for (int x = 0; x < arr.length(); x++) {
//						String guid = arr.getJSONObject(x).getJSONObject("ListItemAllFields").getString("LinkingUrl");
//						System.out.println("guid=" + arr);
//						guids.add(guid);
//					}
//				}

				// get file by specific an uniqueId
				jsonString = SPOnline.get(token, domain, "_api/web/GetFolderByServerRelativeUrl('Shared%20Documents')/Files('sample-excel.xlsx')/$value");
//				if (jsonString != null) {
//					System.out.println(CommonLib.prettyFormatJson(jsonString));
//					json = new JSONObject(jsonString);
//					JSONArray arr = json.getJSONObject("d").getJSONArray("results");
//					System.out.println(arr);
//					for (int x = 0; x < arr.length(); x++) {
//						String guid = arr.getJSONObject(x).getJSONObject("ListItemAllFields").getString("GUID");
//						System.out.println("guid=" + guid);
//						guids.add(guid);
//					}
//				}

//				String filters = "";
//				for (String guid : guids) {
////					String url = "test/_api/web/lists/GetByTitle('doclib1')/items?$select=file,GUID&$filter=" + URLEncoder.encode("GUID eq guid'" + guid + "'", "utf8");
//					if (!filters.equals("")) {
//						filters += " or";
//					}
//					filters += " GUID eq guid'" + guid + "'";
//
////					jsonString = SPOnline.get(token, domain, url);
////					System.out.println(guid + " = " + CommonLib.prettyFormatJson(jsonString));
//				}
//				System.out.println(filters);
//
////				// get all fields
////				HashMap<String, Integer> fieldtypes = new HashMap<String, Integer>();
////				jsonString = SPOnline.get(token, domain, "test/_api/web/lists/GetByTitle('doclib1')/fields");
////				if (jsonString != null) {
////					System.out.println(CommonLib.prettyFormatJson(jsonString));
////					json = new JSONObject(jsonString);
////					JSONArray arr = json.getJSONObject("d").getJSONArray("results");
////					for (int x = 0; x < arr.length(); x++) {
////						System.out.println(arr.getJSONObject(x).getString("Title") + "\t\t\t" + arr.getJSONObject(x).getString("InternalName") + "\t\t\t" + arr.getJSONObject(x).getInt("FieldTypeKind"));
////						fieldtypes.put(arr.getJSONObject(x).getString("InternalName"), arr.getJSONObject(x).getInt("FieldTypeKind"));
////					}
////				}
////
////				String query = "";
////				String expand = "&$expand=";
////
////				// get all fields of a specific view
////				jsonString = SPOnline.get(token, domain, "test/_api/web/lists/GetByTitle('doclib1')/views/getbytitle('" + SPOnline.escapeSharePointUrl(view) + "')/ViewFields");
////				if (jsonString != null) {
////					System.out.println(CommonLib.prettyFormatJson(jsonString));
////					json = new JSONObject(jsonString);
////					JSONArray arr = json.getJSONObject("d").getJSONObject("Items").getJSONArray("results");
////					for (int x = 0; x < arr.length(); x++) {
////						String fieldName = arr.getString(x);
////						if (fieldtypes.get(fieldName) == 20) {
////							query += fieldName + "/Title,";
////							expand += fieldName + ",";
////						} else {
////							query += fieldName + ",";
////						}
////					}
////				}
////				query += "UniqueId,";
////				query += "GUID,";
////
////				if (query.endsWith(",")) {
////					query = query.substring(0, query.length() - 1);
////				}
////				if (expand.endsWith(",")) {
////					expand = expand.substring(0, expand.length() - 1);
////				}
////
//				// get items of a specific view
////				String filters = "";
////				String url = "test/_api/web/lists/GetByTitle('doclib1')/items?$filter=" + URLEncoder.encode("GUID eq guid'47e1eab5-c06e-46a3-a100-ab0fa3874416'", "utf8");
//////				url = "test/_api/web/lists/GetByTitle('doclib1')/items?$filter=" + URLEncoder.encode("ID eq 13", "utf8");
////				url = "test/_api/web/lists/GetByTitle('doclib1')/items?$select=LinkFilename,GUID,UniqueId&$filter=" + URLEncoder.encode("AuthorId eq 11", "utf8");
////				url = "test/_api/web/lists/GetByTitle('doclib1')/items?$select=LinkFilename,GUID,UniqueId&$filter=" + URLEncoder.encode("UniqueId eq guid'e7ac2b9b-d5e3-4257-a21c-0e2951e83887'", "utf8");
////				//url = "test/_api/web/lists/GetByTitle('doclib1')/items?$expand=Unique&$filter=" + URLEncoder.encode("Unique/Id eq guid'efee39c7-ae90-434a-9174-9a9bd7411c74'", "utf8");
//				String url = "test/_api/web/lists/GetByTitle('doclib1')/items?$filters=" + URLEncoder.encode(filters, "utf-8");
//////				url = "test/_api/web/lists/GetByTitle('doclib1')/items;
//				System.out.println(url);
//				jsonString = SPOnline.get(token, domain, url);
//				if (jsonString != null) {
//					System.out.println(CommonLib.prettyFormatJson(jsonString));
//				}
//			} else {
//				System.err.println("Login failed");
//			}
//		} catch (IOException ex) {
//			System.out.println(ex.toString());
//		}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		public static void downloadFile(String url, String outputFileName) throws IOException
	{
		URL url1 = new URL(url);
		try (InputStream in = url1.openStream();
			 ReadableByteChannel rbc = Channels.newChannel(in);
			 FileOutputStream fos = new FileOutputStream(outputFileName)) {
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		}
	}



}
